#ifndef LIST_H
#define LIST_H
#include <cstddef>
#include <iostream>
#include <cassert>
#include <string>

//DECLARACIONES
template <typename T>
class list{
    //ENTIDADES PÚBLICAS
    public:
        list(); //Constructor de la clase lista
        virtual ~list(); //Destructor de la clase lista
        void addFirst(const T & elem); //Procedimiento que agrega un nodo al principio de la lista
        void addLast(const T & elem); //Procedimiento que agrega un nodo al final de la lista
        void add(const T & elem,const unsigned int & i); //Procedimiento que agrega un nodo en la posición indicada *ACLARACIÓN: LA LISTA TIENE COMO PRIMERA POSICIÓN 0*
        unsigned int listLong() const; //Función que devuelve la cantidad de nodos
        bool inList(const T & elem,unsigned int & cost) const; //Función que devuelve si el elemento 'elem' está o no en la lista
        bool lempty() const; //Función que devuelve si la lista está o no vacía
        void del(const T & elem); //Procedimiento que elimina el nodo con el valor 'elem' de la lista *si existiera*
        void delAll(); //Procedimiento que borra todos los nodos
        void showAll(); //Procedimiento que muestra los nodos en orden de posición

    //ENTIDADES PRIVADAS
    private:
        unsigned int llong; //Contador de nodos de la lista
        struct node{ //Estructura de los nodos
            T elem;
            node* next;};
        node* first; //Puntero al principio de la lista
        node* last; //Puntero al final de la lista
};

//IMPLEMENTACIONES
template <typename T> list<T>::list(){ //Constructor
    llong= 0;
    first= NULL;
    last= NULL;}

template <typename T> list<T>::~list(){ //Destructor
    delAll();}

template <typename T> void list<T>::addFirst(const T & elem){
    node* aux = new node;
    if (this-> first == NULL) //Si la lista está vacía
        this-> last = aux;
    aux-> next = this-> first;
    this-> first = aux;
    aux-> elem = elem;
    this-> llong++;}

template <typename T> void list<T>::addLast(const T & elem){
    node* aux = new node;
    aux-> elem = elem;
    aux-> next = NULL;
    if (this-> first == NULL) //Si la lista está vacía
        this-> first = aux;
    else //Si la lista no esta vacía
        this-> last-> next = aux;
    this-> last = aux;
    this-> llong++;}

template <typename T> void list<T>::add(const T & elem,const unsigned int & i){
    assert(i <= (this-> llong)); //ERROR
    if ((i < this-> llong)&& (i != 0)){ //Caso añade en el medio
        node* aux = this-> first;
        for (unsigned int a=0;a<i-1;a++){ //Recorre la lista hasta llegar a la posición indicada
            aux = aux-> next;
        }
        node* aux1 = new node;
        aux1-> next = aux-> next;
        aux-> next = aux1;
        aux1-> elem = elem;
        this-> llong++;
    }else if (i == this-> llong) //Caso añade al final
        addLast(elem);
    else if (i == 0) //Caso añade al principio
        addFirst(elem);}

template <typename T> unsigned int list<T>::listLong() const{
    return this-> llong;}

template <typename T> bool list<T>::inList(const T & elem,unsigned int & cost) const{
    node* aux = this-> first;
    cost = 0;
    while ((aux != NULL) && (aux-> elem != elem)){ //Recorre la lista hasta que se termina o encuentra el elemento
        aux= aux-> next;
        cost++;}
    if (aux == NULL) //Si el elemento existe
        return false;
    else //Si el elemento no existe
        return true;}

template <typename T> bool list<T>::lempty() const{
    return (this -> first == NULL);}

template <typename T> void list<T>::del(const T & elem){
    node* aux = this-> first;
    node* prev = NULL;
    while ((aux != NULL) && (aux-> elem != elem)){ //Recorre la lista hasta que se termina o encuentra el elemento
        prev= aux;
        aux= aux-> next;}
    if (aux != NULL){
        if (aux == this-> first){ //Caso borrar primero
            this-> first = aux-> next;
            if (aux == this-> last) //Caso especial lista con un solo nodo
                this-> last = aux-> next;}
        else{ //Caso borrar medio (o ultimo)
            prev-> next = aux-> next;
            if (aux == this-> last) //Caso borrar ultimo
                this-> last = prev;}
        delete aux;
        this->llong--;}}


template <typename T> void list<T>::delAll(){
    node* aux;
    while (this-> first != NULL){ //Recorre la lista hasta que se termina borrando todos los nodos
        aux = this-> first;
        this-> first = this-> first-> next;
        delete aux;}
    this->llong = 0;}

template <typename T> void list<T>::showAll(){
    node* aux = this-> first;
    while (aux != NULL){ //Recorre la lista hasta que se termina
        std::cout<<aux-> elem << std::endl;
        aux= aux->next;}}

#endif // LIST_H

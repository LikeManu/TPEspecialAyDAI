#include <iostream>
#include <fstream>
#include <string>
#include "list.h"
#include "arbinbus.h"

using namespace std;

void cargar_list(string path, list<string>& l);
void cargar_arbinbus(string path, arbinbus<string>& arbin);
void buscar_list(string pathBusqueda, string pathResultado, list<string> l, const unsigned short int & colec);
void buscar_arbinbus(string pathBusqueda, string pathResultado, arbinbus<string> arbin, const unsigned short int & colec);

int main()
{
    list<string> l1,l2,l3;
    arbinbus<string> arbin1,arbin2,arbin3;
    /*string a;
    cin << a;
    l1.addLast()*/
    bool flag;
    cout << "Autom�tico? (0 = NO)" << endl;
    cin >> flag;
    //Colecci�n 1 en lista
    cout << "Colecci�n 1 en lista";
    if (!flag)
        getchar();
    cargar_list("Datasets\\Coleccion_1.txt",l1);
    buscar_list("Datasets\\Busqueda_1.txt", "Lista_1.txt",l1,1);
    //Colecci�n 1 en arbol
    cout << "Colecci�n 1 en arbol";
    if (!flag)
        getchar();
    cargar_arbinbus("Datasets\\Coleccion_1.txt",arbin1);
    buscar_arbinbus("Datasets\\Busqueda_1.txt", "Arbol_1.txt",arbin1,1);
    //Colecci�n 2 en lista
    cout << "Colecci�n 2 en lista";
    if (!flag)
        getchar();
    cargar_list("Datasets\\Coleccion_2.txt",l2);
    buscar_list("Datasets\\Busqueda_2.txt", "Lista_2.txt",l2,2);
    //Colecci�n 2 en arbol
    cout << "Colecci�n 2 en arbol";
    if (!flag)
        getchar();
    cargar_arbinbus("Datasets\\Coleccion_2.txt",arbin2);
    buscar_arbinbus("Datasets\\Busqueda_2.txt", "Arbol_2.txt",arbin2,2);
    //Colecci�n 3 en lista
    cout << "Colecci�n 3 en lista";
    if (!flag)
        getchar();
    cargar_list("Datasets\\Coleccion_3.txt",l3);
    buscar_list("Datasets\\Busqueda_3.txt", "Lista_3.txt",l3,3);
    //Colecci�n 3 en arbol
    cout << "Colecci�n 3 en arbol";
    if (!flag)
        getchar();
    cargar_arbinbus("Datasets\\Coleccion_3.txt",arbin3);
    buscar_arbinbus("Datasets\\Busqueda_3.txt", "Arbol_3.txt",arbin3,3);
    cout << "FIN";
    getchar();
    return 0;
}

//Abre el txt indicado por la ruta(path), itera por todas las palabras
//y las carga en el contenedor pasado por par�metro
void cargar_list(string path, list<string>& l){
    ifstream origen(path.c_str());
    if (!origen.is_open())
        cout << "No se pudo abrir el archivo: " << path << endl;
    else{
        while (!origen.eof()){
            string palabra;
            origen >> palabra;
            cout << palabra << endl;
            l.addLast(palabra);}}}

void cargar_arbinbus(string path, arbinbus<string>& arbin){
    ifstream origen(path.c_str());
    if (!origen.is_open())
        cout << "No se pudo abrir el archivo: " << path << endl;
    else{
        while (!origen.eof()){
            string palabra;
            origen >> palabra;
            cout << palabra << endl;
            arbin.add(palabra);}}}

void buscar_list(string pathBusqueda, string pathResultado, list<string> l, const unsigned short int & colec){
    bool aux;
    unsigned int costo;
    ifstream busqueda(pathBusqueda.c_str());
    if (!busqueda.is_open())
        cout << "No se pudo abrir el archivo: " << pathBusqueda << endl;
    else {
        ofstream resultado(pathResultado.c_str(), ios::trunc);
        resultado << "COLECCI�N " << colec << endl;
        resultado << "Estructura utilizada: Lista" << endl;
        while (!busqueda.eof()) {
            string palabra;
            busqueda >> palabra;
            aux = l.inList(palabra,costo);
            cout << palabra << endl;
            if (aux)
                resultado << palabra << ", Existe, " << costo << endl;
            else
                resultado << palabra << ", No existe, " << costo << endl;}
        resultado << endl;}}

void buscar_arbinbus(string pathBusqueda, string pathResultado, arbinbus<string> arbin, const unsigned short int & colec){
    bool aux;
    unsigned int costo;
    ifstream busqueda(pathBusqueda.c_str());
    if (!busqueda.is_open())
        cout << "No se pudo abrir el archivo: " << pathBusqueda << endl;
    else {
        ofstream resultado(pathResultado.c_str(), ios::trunc);
        resultado << "COLECCI�N " << colec << endl;
        resultado << "Estructura utilizada: Arbol" << endl;
        while (!busqueda.eof()) {
            string palabra;
            busqueda >> palabra;
            aux = arbin.inArbin(palabra,costo);
            cout << palabra << endl;
            if (aux)
                resultado << palabra << ", Existe, " << costo << endl;
            else
                resultado << palabra << ", No existe, " << costo << endl;}
        resultado << endl;}}
